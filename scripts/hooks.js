// Description:
//   My own stuff

// Dependencies:
//

// Configuration:
//

// Commands:

// Author:
//   lxsameer
const redis = require('redis');
const github = require('../lib/github');
const db = require('../lib/db');

const redisClient = redis.createClient(process.env.REDIS_URL);
redisClient.on("error", function (err) {
    console.log("Error " + err);
});


function withAdmin(robot, res, f) {
  // TODO: Set the admin channel permissions to invite only
  if (
      ['lxsameer', 'sameerynho'].indexOf(res.envelope.user.name) != -1 &&
      res.message.room == '#lxworld'
  ) {
    f();
  }
  else {
    res.reply("You don't have permission to do that.");
  }
}

module.exports = (robot) => {
  robot.redis = redisClient;
  robot.db = db;

  robot.on('disconnect', () => {
    console.log("NNNNNNNNNNNNNNNNNNNNNNNNNNn");
  });

  robot.notifyMe = (msg) => {
    const me = robot.brain.users().sameerynho || robot.brain.users().lxsameer;
    robot.send(me, msg);
  };

  robot.router.post('/webhooks/github/', (req, res) => {
    robot.messageRoom("#lxword", "got something");
    res.send("OK");
  });

  robot.hear(/^> set (.+) (.+) (.+)/, (res) => {
    const moduleName = res.match[1];
    const optionName = res.match[2];
    const optionValue = res.match[3];

    robot.redis.get(moduleName, (err, v) => {
      if (err) {
        throw err;
      }

      const defaultValue = JSON.parse(v || '{}');
      const value = Object.assign(
        defaultValue,
        {[optionName]: optionValue}
      );

      robot.redis.set(moduleName, JSON.stringify(value));
    });

    res.reply('Osu');
  });

  robot.hear(/^> start/ , (res) => {
    withAdmin(robot, res, () => {
      res.reply("Osu");
      github.startAgent(robot);
    });
  });

  robot.hear(/^> stop/ , (res) => {
    withAdmin(robot, res, () => {
      github.stopAgent(robot);
      res.reply("Osu");
    });
  });

};
