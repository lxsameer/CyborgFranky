const jobTable = {};

function scheduleInterval(name, delay, f) {
  const id = setInterval(f, delay);
  jobTable[name] = id;
  return id;
}

function cancelJob(name) {
  const job = jobTable[name];
  return job ? clearTimeout(job) && true : null;
}

module.exports = {
  scheduleInterval,
  cancelJob
};
