const jobs = require('./jobs');
const Octokit = require("@octokit/rest");


const client = new Octokit({
  // see "Authentication" section below
  auth: `token ${process.env.GITHUB_TOKEN}`,

  // setting a user agent is required: https://developer.github.com/v3/#user-agent-required
  // v1.2.3 will be current @octokit/rest version
  userAgent: "octokit/rest.js v1.2.3",

  // add list of previews you’d like to enable globally,
  // see https://developer.github.com/v3/previews/.
  // Example: ['jean-grey-preview', 'symmetra-preview']
  previews: [],

  // set custom URL for on-premise GitHub Enterprise installations
  baseUrl: "https://api.github.com",

  // pass custom methods for debug, info, warn and error
  log: {
    debug: () => {},
    info: () => {},
    warn: console.warn,
    error: console.error
  },

  request: {
    // Node.js only: advanced request options can be passed as http(s) agent,
    // such as custom SSL certificate or proxy settings.
    // See https://nodejs.org/api/http.html#http_class_http_agent
    agent: undefined,

    // request timeout in ms. 0 means no timeout
    timeout: 0
  }
});

const reasons = [
  "mention",
  "assign",
  "author",
  "comment",
  "review_requested",
  "team_mention"
];

function processNotifications(result) {
  return result.data
    .filter(x => reasons.indexOf(x.reason) != -1)
    .reduce((acc, x) => {
      acc[x.reason] = acc[x.reason] || [];
      acc[x.reason].push(x);
      return acc;
    }, {});
}

function getConfig(robot) {
  return new Promise((resolve,reject) => {
    robot.redis.get('github', (err, v) => {
      err ? reject(err) : resolve(JSON.parse(v || '{}'));
    });
  });
};

function getGithubStatus(robot) {
  return new Promise((resolve,reject) => {
    robot.redis.get('github_state', (err, v) => {
      err ? reject(err) : resolve(JSON.parse(v || '{}'));
    });
  });
};

function checkGithub(robot) {
  const owner = "udemy";
  const repo = "website-django";
  const participating = true;
  const all = false;

  console.info("Fetching notifications...");

  return client.activity
    .listNotificationsForRepo({
      owner,
      repo,
      all,
      participating
    })
    .then(data => {
      const result = processNotifications(data);
      const counts = Object.keys(result).reduce((acc, x) => {
        acc[x] = result[x] ? result[x].length : 0;
        return acc;
      }, {});

      robot.redis.set("github_state", JSON.stringify(counts));
      console.info('Notification state has been set.');
    });
}


function mentionNotifier(robot) {
  return async () => {
    console.log("Checking for `mention` status.");

    const state = await getGithubStatus(robot);
    const mentionCounts = state.mention;

    console.log(`Mention status is <${mentionCounts}>.`);

    if (mentionCounts && mentionCounts > 0) {
      robot.notifyMe(`Dude, you have ${counts.mention}(s) on github.`);
    }

  };
}

function statusNotifier(robot) {
  return async () => {
    console.log("Checking for all the notifications.");

    const state = await getGithubStatus(robot);
    const finalResult = Object.keys(state)
          .map(x => `${x}: ${state[x]}`)
          .join(', ');

    robot.messageRoom("#lxworld", finalResult);
  };
}


function fetcherJob(robot) {
  return async () => {
    checkGithub(robot);
  };
}

async function startAgent(robot) {
  const config = await getConfig(robot) || {};
  const fetchDelay = config.fetchDelay || 15000;
  const notifierDelay = config.notifierDelay || 20000;

  jobs.scheduleInterval(
    'githubFetcher',
    fetchDelay,
    fetcherJob(robot)
  );

  jobs.scheduleInterval(
    'githubMentions',
    fetchDelay,
    mentionNotifier(robot)
  );

  jobs.scheduleInterval(
    'githubStatus',
    notifierDelay,
    statusNotifier(robot)
  );
}

function stopAgent(robot) {
  jobs.cancelJob('githubFetcher');
  jobs.cancelJob('githubMentions');
  jobs.cancelJob('githubStatus') ;
}

module.exports = {
  startAgent,
  stopAgent,
};
