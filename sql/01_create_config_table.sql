CREATE TABLE IF NOT EXISTS config (
       id serial primary key,
       key_name varchar(255) not null,
       value text not null,
       created_at timestamp not null,
       modified_at timestamp not null
)
